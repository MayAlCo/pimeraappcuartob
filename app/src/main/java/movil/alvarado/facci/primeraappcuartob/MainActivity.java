package movil.alvarado.facci.primeraappcuartob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonBuscar, buttonGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        buttonLogin =(Button)findViewById( R.id.buttonLogin );
        buttonBuscar =(Button)findViewById( R.id.buttonBuscar);
        buttonGuardar =(Button)findViewById( R.id.buttonGuardar );





       buttonGuardar.setOnClickListener( new View.OnClickListener(){
           @Override
           public void onClick(View view) {
               Intent intent = new Intent( MainActivity.this, ActividadGuardar.class );
               startActivity( intent );
           }
       } );

        buttonBuscar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity( intent );
            }
        } );
        buttonLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, ActividadLogin.class );
                startActivity( intent );

            }



        } );
    }
}

package movil.alvarado.facci.primeraappcuartob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class ActividadBuscar extends AppCompatActivity {

    EditText etCedula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_actividad_buscar );

        etCedula = (EditText)findViewById( R.id.etCedula );
    }
}
